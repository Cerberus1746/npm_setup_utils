npm_setup_utils [![Build Status](https://travis-ci.org/Cerberus1746/npm_setup_utils.svg?branch=development)](https://travis-ci.org/Cerberus1746/npm_setup_utils)
===============

Makes your setup.py be able to read a package.json file.
